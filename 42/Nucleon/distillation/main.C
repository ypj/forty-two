#include <stdio.h>
#include <stdlib.h>
#include <util/lattice.h>
#include <util/gjp.h>
#include <util/verbose.h>
#include <util/error.h>
#include <alg/alg_fix_gauge.h>
#include <alg/alg_plaq.h>
#include <alg/alg_nuc3pt.h>
#include <util/command_line.h>
#include <util/ReadLatticePar.h>
//#include <util/lattice.h>
//#include <util/smalloc.h>
//#include <util/pmalloc.h>
//#include <alg/alg_base.h>
#include <alg/alg_meas.h>
//#include <alg/common_arg.h>
#include <alg/qpropw.h>

#include "distil.h"


USING_NAMESPACE_CPS

int main(int argc,char *argv[])
{
  char *cname = argv[0] ;
  char *fname = "main()" ;
  char *filename;
  filename = (char*) smalloc( 128*sizeof(char) );

  CommandLine::is(argc,argv);

  int serial_io = 0;
  int concur_io_number = 64;
  char log_dir[255];
  sprintf(&log_dir[0],"IOLOG");

  // working directory
  //const char* dir(CommandLine::arg());
  const char* dir(".");
  FILE *fp;
  
  DoArg do_arg;
  strcpy(filename,dir);
  strcat(filename,"/do_arg.vml");
  do_arg.Decode(filename,"do_arg");
  strcpy(filename, dir);
  strcat(filename,"/do_arg.dat");
  do_arg.Encode(filename,"do_arg");
  
  CommonArg c_arg;
  sprintf(filename,"%s/nucl.dat",dir);
  c_arg.set_filename(filename);

  //if( (fp = Fopen((char *)c_arg.results, "a")) == NULL ) {
  //  ERR.FileA(cname, fname, (char *)c_arg.results );
  //}
  
  strcpy(filename, dir);
  strcat(filename,"/out.dat");
  if( (fp = Fopen(filename, "w")) == NULL ) {
    ERR.FileA(cname, fname, filename );
  }
  
  Start(&argc,&argv);
  GJP.Initialize(do_arg);
  LRG.Initialize();

  Fprintf( fp, "Machine size in nodes:  (X,Y,Z,T) = (%d,%d,%d,%d)\n",
    SizeX(), SizeY(), SizeZ(), SizeT() );
  Fprintf( fp, "\nCommand line arguments\n");
  Fprintf( fp, "\tprogram name:\t%s\n", argv[0] );
  Fflush(fp);
  
  Distillation dist;
  //dist.readHalfPerambulator(0,0,0);
  //dist.readHalfPerambulator(0,0,0);
  //dist.readHalfPerambulator(1,1,13);
  //dist.readHalfPerambulator(2,2,37);
  //dist.readHalfPerambulator(3,3,59);

  //Lattice &lat = LatticeFactory::Create(F_CLASS_WILSON, G_CLASS_WILSON);
  GwilsonFwilson lat;

#if 0 // method 1
  QPropWFactory::Create();
  QPropWFactory::Destroy();
#endif 
#if 0 // method 2
  QPropWArg qp_arg;
  qp_arg.file = "distil_prop.0";
  QPropW *qp = new QPropW(lat, &c_arg);
  qp->Allocate(PROP);
  //qp->RestoreQProp(qp_arg.file,PROP);
  qp->Delete(PROP);
  delete qp;
#endif 
#if 1 // method 3
  QPropW quark = QPropW(lat,&c_arg);
  quark.Allocate(PROP);

  //quark[site_idx].d[snk].c[snk].d[src].c[src]
  //su3_vector tmpV;
  //tmpV = quark[site_idx].d[snk].c[snk].d[src];
  WilsonMatrix wm;
  Complex tmpC;
  double *perambulator = new double [2*GJP.Sites(3)];
  
  // source 
  int t0, site_idx0;
  std::vector<int> coor0(4);

  int site_idx;
  std::vector<int> coor(4);
  
  std::vector<int> dim(4);
  
  for (int i=0; i<4; ++i) {
    dim[i] = GJP.NodeSites(i);
  }
  
  t0 = 0;
  coor0[3] = t0;
  
  for (int i=0; i<GJP.VolNodeSites(); ++i) {
    quark[i] = 0.;
  }
  

//  b0 = 0;

//  for (int s0=0; s0<4; ++s0) {
//    dist.readHalfPerambulator(t0,s0,b0);
//
//    for (int z=0; z<GJP.NodeSites(2); ++z) { 
//      coor0[2] = z;
//      for (int y=0; y<GJP.NodeSites(1); ++y) { 
//        coor0[1] = y;
//        for (int x=0; x<GJP.NodeSites(0); ++x) { 
//          coor0[0] = x;
//
//          dist.IndexFromCoor(coor0,site_idx0,dim);
//          //wm = quark[site_idx];
//          
//          for (int c0=0; c0<3; ++c0) {
//            for (int s=0; s<4; ++s) {
//              for (int c=0; c<3; ++c) {
//                //tmpC = dist.h.field(site_idx,s,c) * dist.v.field(b0,site_idx,c0);
//                //wm(s,c,s0,c0, tmpC);
//                quark[site_idx0](s,c,s0,c0) 
//                  += dist.h.field(site_idx0,s,c) * std::conj(dist.v.field(b0,site_idx0,c0));
//              } // c
//            } // s
//          } // c0
//
//        } // x
//      } // y
//    } // z
//    
//  } // s0

//  int cnt=0; 
//  for (int s0=0; s0<4; ++s0) {
//    for (int b0=0; b0<dist.v.num_basis; ++b0) {
//      dist.readHalfPerambulator(t0,s0,b0);
//      for (int site_idx1=0; site_idx1<GJP.VolNodeSites(); ++site_idx1) { 
//        for (int s1=0; s1<4; ++s1) {
//          for (int c1=0; c1<3; ++c1) {
//            for (int b1=0; b1<dist.v.num_basis; ++b1) {
//              for (int c=0; c<3; ++c) {
//                for (int z=0; z<GJP.NodeSites(2); ++z) { 
//                  coor0[2] = z;
//                  for (int y=0; y<GJP.NodeSites(1); ++y) { 
//                    coor0[1] = y;
//                    for (int x=0; x<GJP.NodeSites(0); ++x) { 
//                      coor0[0] = x;
//                      dist.IndexFromCoor(coor0,site_idx0,dim);
//                      for (int c0=0; c0<3; ++c0) {
//                        quark[site_idx1](s1,c1,s0,c0) += (0.,0); 
//                        //dist.v.field(b1,site_idx1,c1) * std::conj(dist.v.field(b1,site_idx1,c)) 
//                        //* dist.h.field(site_idx1,s1,c)
//                        //* std::conj(dist.v.field(b0,site_idx0,c0));
//                        cnt++;
//                      }
//                    }
//                  }
//                }
//              }
//            }
//          }
//        }
//      }
//      if( UniqueID()==0) std::cout << "ckpt C: " << cnt << std::endl; 
//    }
//  }

  for (int s0=0; s0<4; ++s0) {
    for (int b0=0; b0<dist.v.num_basis; ++b0) {

      dist.readHalfPerambulator(t0,s0,b0);
      
      for (int s1=0; s1<4; ++s1) {
        for (int b1=0; b1<dist.v.num_basis; ++b1) {
          
          for (int i=0; i<2*GJP.Sites(3); ++i) {
            perambulator[i] = 0.;
          }
          
          for (int t=0; t<GJP.NodeSites(3); ++t) { 
            coor[3] = t;
            tmpC = (0.,0.);
            for (int z=0; z<GJP.NodeSites(2); ++z) { 
              coor[2] = z;
              for (int y=0; y<GJP.NodeSites(1); ++y) { 
                coor[1] = y;
                for (int x=0; x<GJP.NodeSites(0); ++x) { 
                  coor[0] = x;
                  dist.IndexFromCoor(coor,site_idx,dim);
                  for (int c=0; c<3; ++c) {
                    tmpC += std::conj(dist.v.field(b1,site_idx,c)) * dist.h.field(site_idx,s1,c);
                  }
                }
              }
            }
            perambulator[2*(GJP.NodeCoor(3)*GJP.NodeSites(3)+t)] = tmpC.real();
            perambulator[2*(GJP.NodeCoor(3)*GJP.NodeSites(3)+t)+1] = tmpC.imag();
          }

          glb_sum(perambulator,2*GJP.Sites(3));
          if( UniqueID()==0) {
            for (int t_true=0; t_true<GJP.Sites(3); ++t_true) {
              std::cout << "ckpt C"
                        << ": s1=" << s1
                        << ", b1=" << b1
                        << ", s0=" << s0
                        << ", b0=" << b0
                        << ", t=" << t_true 
                        << ", peramb=" << perambulator[2*t_true] 
                        << ", " << perambulator[2*t_true+1] << std::endl; 
            }
          }

        }
      }

    }
  }
  
#endif 
  
  // calculate the nucleon correlation functions
  // ypj [fixme] need new class 
  // AlgDistnContract alg_dc(lattice,&common_arg,&nuc3pt_arg);
  // dc.run();
  
  //LatticeFactory::Destroy();
  
  delete [] perambulator;
  End();
  
  return 0;
  
  
  // enable the link buffer
  //lattice.EnableLinkBuffer(1000);
  
  //const char* conf_file(CommandLine::arg());
  //if( conf_flag ) do_arg.start_conf_kind = START_CONF_FILE;

  //NoArg no_arg;

  

  
  // ypj [fixme] read perambulators and distillation sources 
  //             insteads of a gauge conf file
  
  //if(do_arg.start_conf_kind == START_CONF_FILE){
  //   
  //  ReadLatticeParallel rd;
  //  rd.setLogDir(&log_dir[0]);
  //
  //  char confname[100];
  //  sprintf(confname,"%s",conf_file);
  //  printf("%s %s\n",conf_file,confname);
  //  QioArg rd_arg(confname,1e-6);
  //  rd_arg.ConcurIONumber=concur_io_number;
  //  if(serial_io) rd.setSerial();

  //  cout<<"Start loading lattice"<<endl;
  //  rd.read(lattice,rd_arg);
  //  if(!rd.good()) ERR.General(cname,fname,"Failed to load lattice\n");
  //  cout<<"Loading latice finished"<<endl;
  //  Fprintf( fp, "\tlattice filename:\t%s\n",confname );
  //}

  //Fclose(fp);//output file

  //AlgPlaq     plaq   (lattice,&common_arg,&no_arg);
  //plaq.run();

  // args for the nucleon correlation functions:
  //Nuc3ptArg nuc3pt_arg;
  // load defaults
  //strcpy(filename, dir);
  //strcat(filename,"/nuc3pt_arg.default");
  //nuc3pt_arg.Decode(filename,"nuc3pt_arg");
  //nuc3pt_arg.ensemble_id = 0;
  //we cannot calculate conserved current with 4D prop.
  //if(nuc3pt_arg.calc_QProp==0) nuc3pt_arg.DoConserved=0;
  //sprintf(filename,"%s/nuc3pt_arg.dat",dir);
  //nuc3pt_arg.Encode(filename,"nuc3pt_arg");

  // compute gauge-fixing matrices for use with momentum source
  //if(nuc3pt_arg.src_type == ( SourceType ) 3 ){ // BOX = 3
  //  FixGaugeArg fix_arg;
  //  fix_arg.fix_gauge_kind = ( FixGaugeType )  3; // Coulomb time slices
  //  fix_arg.hyperplane_start = nuc3pt_arg.t_source;
  //  fix_arg.hyperplane_step =  nuc3pt_arg.source_inc;
  //  fix_arg.hyperplane_num =  nuc3pt_arg.num_src;
  //  fix_arg.max_iter_num =  20000;
  //  fix_arg.stop_cond    =  1e-8;
  //  /* FIX the gauge */
  //  if(fix_arg.fix_gauge_kind != FIX_GAUGE_NONE){
  //     AlgFixGauge fix_gauge(lattice,&common_arg,&fix_arg);
  //     fix_gauge.run();
  //  }
  //  if( (fp = Fopen((char *)common_arg.results, "a")) == NULL ) {
  //    ERR.FileA(cname, fname, (char *)common_arg.results );
  //  }
  //  Fprintf( fp, "Coulomb gauge fixing matrices computed\n");
  //  Fprintf( fp, "start = %d  step = %d num = %d\n", 
  //           fix_arg.hyperplane_start,
  //           fix_arg.hyperplane_step,
  //           fix_arg.hyperplane_num);
  //  Fprintf( fp, "max it = %d  tol = %g\n", 
  //           fix_arg.max_iter_num,
  //           fix_arg.stop_cond);
  //  Fclose(fp);//output file
  //} 


  
  //AlgNuc3pt alg_nuc3pt(lattice,&common_arg,&nuc3pt_arg);
  //alg_nuc3pt.run() ;
  
  //End();

  //return 0;
} 
