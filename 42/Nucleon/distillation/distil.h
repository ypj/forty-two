#ifndef INCLUDED_DISTIL_H
#define INCLUDED_DISTIL_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <util/gjp.h>
#include <alg/wilson_matrix.h>

CPS_START_NAMESPACE

struct distillation_basis {
  std::vector<int> lattice_size;
  std::vector<int> node_geometry;
  int num_basis = 0;
  unsigned long long data_read_size = 0;
  unsigned long long data_size = 0;
  unsigned long long *data_fpos = NULL;
  su3_vector **data = NULL;
  int t = 0;
  unsigned long long* fpos(int t, int i) { 
    return data_fpos + (i + num_basis*t);
  }
  //su3_vector* field(int i){
  //  return data[i];
  //}
  Complex& field(int basis, int offset, int color){
    return data[basis][offset].c[color];
  }
  //double* field(int t, int i){
  //  return data + (i + num_basis*data_size*t);
  //}
};
typedef struct distillation_basis DistillationBasis;


struct half_perambulator {
  std::vector<int> lattice_size;
  std::vector<int> node_geometry;
  int num_basis = 0;
  int num_src = 0;
  std::vector<int> src_t;
  unsigned long long data_read_size = 0;
  unsigned long long data_size = 0;
  unsigned long long *data_fpos = NULL;
  WilsonVector *data = NULL;
  int prec = 0;
  // loaded field in the data pointer
  int cache_t = -1; // source time index
  int cache_i = -1; // basis index
  int cache_s = -1; // spin index
  unsigned long long* fpos(int t, int s, int i) { 
    return data_fpos +  s + 4*(i + num_basis*t);
  }
  Complex& field(int offset, int spin, int color){
    return data[offset].d[spin].c[color];
  }
  //double* field(int t, int i, int s){
  //  return data + data_size*(s + 4*(i + num_basis*t));
  //}
};
typedef struct half_perambulator HalfPerambulator;


//struct perambulator {
//  std::vector<int> lattice_size;
//  std::vector<int> node_geometry;
//  unsigned long long data_size;
//  double *p;
//};
//typedef struct perambulator Perambulator;


class Distillation
{
  private:
    std::string arg_file;
    std::string storage;
    std::vector<std::string> half_peramb_file;

    std::vector<int> lattice_size;
    std::vector<int> node_geometry;
    unsigned long long data_size;
    
    unsigned long long lookupTag(std::ifstream &f, std::string query, 
                                 std::vector<int> &geom,
                                 unsigned long long *len, 
                                 unsigned long long *pos);
    void setIndex();
    void readBasis(void);
    



  public:
    DistillationBasis v;
    HalfPerambulator h;
    //Perambulator p;
    
    void readHalfPerambulator(int src_t, int spin, int basis);

    static inline void CoorFromIndex (std::vector < int >&coor, int index,
        			      std::vector < int >&dims)
    {
      int nd = dims.size ();
        coor.resize (nd);
      for (int d = 0; d < nd; d++)
      {
        coor[d] = index % dims[d];
        index = index / dims[d];
      }
    }
  
    static inline void IndexFromCoor (std::vector < int >&coor, int &index,
        			      std::vector < int >&dims)
    {
      int nd = dims.size ();
      int stride = 1;
      index = 0;
      for (int d = 0; d < nd; d++) {
        index = index + stride * coor[d];
        stride = stride * dims[d];
      }
    }
    
    Distillation();
    ~Distillation();
};



CPS_END_NAMESPACE

#endif
