#!/bin/bash

CPS=${HOME}/work/cps/cps_pp
SciDAC=${HOME}/work/SciDAC
FFTW=/opt/fftw/v3.3.7
GSL=${HOME}/work/gsl/install/v
BUILD=${HOME}/work/lqcd-builds/cps-scidac-gcc7-openmpi

QMP_PREFIX=${BUILD}
QIO_PREFIX=${BUILD}
CPS_PREFIX=${BUILD}

# override openmpi's wrapper compiler
export OMPI_CC=gcc-7
export OMPI_CXX=g++-7

next=$1
: ${next:="--lib"}

function terminate()
{
  if [ ! $1 ]; then
    echo "Terminate ${next}"
    echo "Terminate ${@:2-}"
    exit $1
  fi
}

#QMP install
if [ "$next" = "--qmp" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qmp-2.3.1_SSE
  make distclean
  sh configure --prefix=$QMP_PREFIX \
               --with-qmp-comms-type=MPI \
               CC=mpicc \
               CFLAGS="-Wall -std=c99 -O3"
  terminate $? !!
  make
  terminate $? !!
  make CTAGS
  terminate $? !!
  make install
  terminate $? !!
fi


#QIO install
if [ "$next" = "--qio" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qio-2.3.9
  make distclean
  sh configure --prefix=$QIO_PREFIX \
               --with-qmp=$QMP_PREFIX \
               --enable-qmp-route \
               --enable-fast-route \
               --enable-largefile \
               CFLAGS="-Wall -std=c99 -O3"
  terminate $? !!
  make
  terminate $? !!
  make CTAGS
  terminate $? !!
  make install
  terminate $? !!
fi


#CPS install
if [ "$next" = "--cps" ] || [ "$next" = "--lib" ]; then
#  cd ${CPS}
#  make realclean
#  cd -
  sh ${CPS}/configure \
     --enable-qmp=$QMP_PREFIX \
     --enable-qio=$QIO_PREFIX \
     --disable-bfm \
     CC=mpicc CXX=mpiCC \
     CFLAGS="-Wno-write-strings -I${FFTW}/include -I${GSL}/include" \
     CXXFLAGS="-Wno-write-strings -I${FFTW}/include -I${GSL}/include"
  terminate $? !!
  make vml
  terminate $? !!
  make 
  terminate $? !!
fi


if [ "$next" = "--nucleon-dist" ] || [ "$next" = "--app" ]; then
  cd ../42/Nucleon/distillation
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? !!
  make clean
  terminate $? !!
  make
  terminate $? !!
  cp NOARCH.x ${CPS_PREFIX}/bin/nucleon-dist
  terminate $? !!
fi

if [ "$next" = "--evol-force" ] || [ "$next" = "--app" ]; then
  cd ../42/evol/force
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? !!
  make clean
  terminate $? !!
  make
  terminate $? !!
  cp NOARCH.x ${CPS_PREFIX}/bin/evol-force
  terminate $? !!
fi
