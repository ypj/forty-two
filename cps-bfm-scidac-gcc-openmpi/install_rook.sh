#!/bin/bash

CPS=${HOME}/work/cps/cps_pp
SciDAC=${HOME}/work/SciDAC
FFTW=/opt/fftw/v3.3.7
GSL=${HOME}/work/gsl/install/v
XML=${HOME}/work/libxml2
BAGEL=${HOME}/work/bagel-3.3
BFM=${HOME}/work/bfm-3.4
BUILD=$(pwd)

QMP_PREFIX=${BUILD}
QIO_PREFIX=${BUILD}
QLA_PREFIX=${BUILD}
XML_PREFIX=${BUILD}
QDP_PREFIX=${BUILD}
CPS_PREFIX=${BUILD}
BAGEL_PREFIX=${BUILD}
BFM_PREFIX=${BUILD}

# override openmpi's wrapper compiler
#export OMPI_CC=gcc-7
#export OMPI_CXX=g++-7

next=$1
: ${next:="--lib"}

function terminate()
{
  if [ ! $1 ]; then
    exit "Exit $1 from ${next}"
  fi
}

#QMP install
if [ "$next" = "--qmp" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qmp-2.3.1_SSE
  make distclean
  sh configure --prefix=$QMP_PREFIX \
               --with-qmp-comms-type=MPI \
               CC=mpicc \
               CFLAGS="-Wall -std=c99 -O3"
  terminate $? 
  make
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#QIO install
if [ "$next" = "--qio" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qio-2.3.9
  make distclean
  sh configure --prefix=$QIO_PREFIX \
               --with-qmp=$QMP_PREFIX \
               --enable-qmp-route \
               --enable-fast-route \
               --enable-largefile \
               CFLAGS="-Wall -std=c99 -O3"
  terminate $? 
  make
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#QLA install
if [ "$next" = "--qla" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qla-1.7.0-a7
  QLA_CFLAGS=""
  QLA_CFLAGS="${QLA_CFLAGS} -Wall -std=c99" 
  QLA_CFLAGS="${QLA_CFLAGS} -O3 -march=native" 
  QLA_CFLAGS="${QLA_CFLAGS} -ffast-math" 
  QLA_CFLAGS="${QLA_CFLAGS} -funroll-loops" 
  QLA_CFLAGS="${QLA_CFLAGS} -fprefetch-loop-arrays"
  QLA_CFLAGS="${QLA_CFLAGS} -fomit-frame-pointer"
  make distclean
  sh configure --prefix=$QLA_PREFIX \
               --enable-nc=3 \
               --enable-temp-precision=D \
               CC="gcc" \
               CFLAGS="${QLA_CFLAGS}"
               LDFLAGS=-static
  terminate $? 
  make -j 4
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#XML2 install
if [ "$next" = "--xml" ] || [ "$next" = "--lib" ]; then
  cd $XML
  make distclean
  sh configure --prefix=$XML_PREFIX \
               CFLAGS=" "
  terminate $? 
  make
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#QDP++ install
if [ "$next" = "--qdp++" ] || [ "$next" = "--lib" ]; then
  cd $SciDAC/qdp++-1.36
  make distclean
  sh configure --prefix=$QDP_PREFIX \
               --with-qmp=$QMP_PREFIX \
               --with-libxml2=$XML_PREFIX \
               --enable-parallel-arch=scalar \
               --enable-precision=double \
               --enable-nc=3 \
               --enable-sse \
               --enable-sse2 \
               CC=mpicc CXX=mpiCC \
               CXXFLAGS="-Wall -O3 -fpermissive -march=native" \
               CFLAGS="${CXXFLAGS}"
  terminate $? 
  make
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#BAGEL install
if [ "$next" = "--bagel" ] || [ "$next" = "--lib" ]; then
  cd $BAGEL
  make distclean
  sh configure --prefix=$BAGEL_PREFIX \
               CFLAGS=" "
  terminate $? 
  make
  terminate $? 
  make CTAGS
  terminate $? 
  make install
  terminate $? 
fi


#BFM install
if [ "$next" = "--bfm" ] || [ "$next" = "--lib" ]; then
  #PATH=${PATH}:${QDP_PREFIX}/bin
  #export PATH
  cd $BFM
  make distclean
  sh configure --prefix=$BFM_PREFIX \
               --enable-comms=QMP \
               --with-bagel=${BAGEL_PREFIX} \
               --enable-qdp \
               CC=mpicc CXX=mpiCC \
               CFLAGS="-I${GSL}/include -I${QDP_PREFIX}/include -I${XML_PREFIX}/include/libxml2 -fpermissive" \
               CPPFLAGS="-I${GSL}/include -I${QDP_PREFIX}/include -I${XML_PREFIX}/include/libxml2 -fpermissive" \
               CXXFLAGS="-I${GSL}/include -I${QDP_PREFIX}/include -I${XML_PREFIX}/include/libxml2 -fpermissive"
  
  terminate $? 
  make
  terminate $? 
  #make CTAGS
  #terminate $? 
  #make install
  #terminate $? 
fi


#CPS install
if [ "$next" = "--cps" ] || [ "$next" = "--lib" ]; then
  #cd ${CPS}
  #make realclean
  sh ${CPS}/configure \
     --enable-qmp=$QMP_PREFIX \
     --enable-qio=$QIO_PREFIX \
     --enable-bfm \
     CC=mpicc CXX=mpiCC \
     CFLAGS="-Wno-write-strings -I${FFTW}/include -I${GSL}/include -I${BFM}/bfm" \
     CXXFLAGS="-Wno-write-strings -I${FFTW}/include -I${GSL}/include -I${BFM}/bfm"
  terminate $? 
  make vml
  terminate $? 
  make
  terminate $? 
fi


if [ "$next" = "--nucleon-dist" ] || [ "$next" = "--app" ]; then
  cd ../42/Nucleon/distillation
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? 
  make clean
  terminate $? 
  make
  terminate $? 
  cp NOARCH.x ${CPS_PREFIX}/bin/nucleon-dist
  terminate $? 
fi


if [ "$next" = "--evol-force" ] || [ "$next" = "--app" ]; then
  cd ../42/evol/force
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? 
  make clean
  terminate $? 
  make
  terminate $? 
  cp NOARCH.x ${CPS_PREFIX}/bin/evol-force
  terminate $? 
fi
