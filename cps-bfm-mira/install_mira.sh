#!/bin/bash
#x export DARSHAN_DISABLE=1
#x export PPCF=/bgsys/drivers/ppcfloor
#x #export PPCF=/bgsys/drivers/V1R2M2/ppc64
#x export LEGACY=${PPCF}/comm/gcc.legacy/bin/
#x export  BASE=/gpfs/mira-home/chulwoo/V10.0/
#x export  PATH=${PPCF}/gnu-linux/bin:$PATH
#x export  PATH=${PPCF}/comm/gcc/bin:$PATH
#x export MPICH_CC=${PPCF}/gnu-linux-4.7.2/bin/powerpc64-bgq-linux-gcc
#x export MPICH_CXX=${PPCF}/gnu-linux-4.7.2/bin/powerpc64-bgq-linux-g++
#x #export  PATH=${PPCF}/comm/gcc.legacy/bin:$PATH
#x export  INSDIR=${BASE}/install_CJ_6
#x export  INST=${BASE}/install_CJ_6
#x export  PATH=${INSDIR}/bin/:$PATH
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
source ~/work/setup6.sh

echo "-----"
echo ${PATH}
echo "-----"
gcc -v
echo "-----"
mpicc -v 
echo "-----"
mpicxx -v
echo "-----"
#exit 0

#CPS=${HOME}/work/cps/cps_pp
CPS=/gpfs/mira-home/ypjang/work/cps/cps_pp
#SciDAC=${HOME}/work/SciDAC
#FFTW=/opt/fftw/v3.3.7
#GSL=${HOME}/work/gsl/install/v
#XML=${HOME}/work/libxml2
#BAGEL=${HOME}/work/bagel-3.3
#BFM=${HOME}/work/bfm-3.4
#BUILD=$(pwd)
BUILD=/gpfs/mira-home/ypjang/work/forty-two/cps-bfm-mira


QMP_PREFIX=${INSDIR}
QIO_PREFIX=${INSDIR}
#QLA_PREFIX=${BUILD}
#XML_PREFIX=${INSDIR}/include/libxml2
XML_PREFIX=${INSDIR}
QDP_PREFIX=${INSDIR}
CPS_PREFIX=${BUILD}
BAGEL_PREFIX=${INSDIR}
BFM_PREFIX=${INSDIR}/bfm-zMobius-fixed

# override openmpi's wrapper compiler
#export OMPI_CC=gcc-7
#export OMPI_CXX=g++-7

next=$1
: ${next:="--lib"}

function terminate()
{
  if [ ! $1 ]; then
    exit "Exit $1 from ${next}"
  fi
}


#CPS install
if [ "$next" = "--cps" ] || [ "$next" = "--lib" ]; then
  #cd ${CPS}
  #make realclean
  sh ${CPS}/conf --enable-target=bgq
  terminate $? 
  sh ${CPS}/configure \
     --build=powerpc64-unknown-linux-gnu \
     --host=powerpc64-bgq-linux \
     --enable-target=bgq \
     --enable-c11 \
     --enable-gmp \
     --enable-c11-rng \
     --enable-qmp=$QMP_PREFIX \
     --enable-qio \
     --enable-xml=$XML_PREFIX \
     --enable-bfm=$BFM_PREFIX \
     CXX=mpicxx 
  terminate $? 
  make vml
  terminate $? 
  make
  terminate $? 
fi


if [ "$next" = "--nucleon-dist" ] || [ "$next" = "--app" ]; then
  mkdir -p bin
  cd ../42/Nucleon/distillation
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? 
  make clean
  terminate $? 
  make
  terminate $? 
  cp BGQ.x ${CPS_PREFIX}/bin/nucleon-dist
  terminate $? 
fi


if [ "$next" = "--evol-force" ] || [ "$next" = "--app" ]; then
  mkdir -p bin
  cd ../42/evol/force
  cp ${CPS_PREFIX}/Makefile.users .
  terminate $? 
  make clean
  terminate $? 
  make
  terminate $? 
  cp BGQ.x ${CPS_PREFIX}/bin/evol-force
  terminate $? 
fi
